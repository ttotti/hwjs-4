// Завдання №1

let number1;
let number2;

do {
  number1 = prompt('Перше число:');
} while (isNaN(Number(number1)));

do {
  number2 = prompt('Друге число:');
} while (isNaN(Number(number2)));

if (number1 < number2) {
  for (let num = number1; num <= number2; num++) {
    console.log(num);
  }
} else {
  for (let num = number2; num <= number1; num++) {
    console.log(num);
  }
}

// Завдання №2

let number;

do {
  number = prompt('Введіть число:');
  number = Number(number);
} while (isNaN(number) || number % 2 !== 0);

console.log(`Ваше число: ${number}`);
